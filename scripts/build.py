#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-2.0-or-later

import sys
import os
import shutil
import subprocess
import json
import pathlib
import stat
import shlex
import glob
import mesonrpath

BUILD_SYSTEMS = ['make', 'cmake', 'autotools', 'meson', 'scons']

def build (build_system, constituent_path, package_path, tag, configure_flags, make_options, cmake_prefix_path, rpath):

    if not build_system in BUILD_SYSTEMS:
        print("'{0}' is not in the supported build system list [{1}]".format(build_system, ",".join(BUILD_SYSTEMS)))
        raise

    cmake_path = shutil.which("cmake3")
    if not cmake_path:
        cmake_path = shutil.which("cmake")
    ninja_path = shutil.which("ninja")
    meson_path = shutil.which("meson")
    python_path = shutil.which("python3")

    if build_system == "make":
        if os.path.isfile("configure"):
            if configure_flags:
                print("Using '{}' as configure flags".format(" ".join(configure_flags)))
            subprocess.run(["./configure",
                "--prefix={}".format(os.path.join(package_path, tag))] + configure_flags, check=True)
        subprocess.run(["make"] + make_options, check=True)
        subprocess.run(["make", "install"], check=True)
    elif build_system == "autotools":
        if configure_flags:
            print("Using '{}' as configure flags".format(" ".join(configure_flags)))
        subprocess.run(["./configure",
            "--libdir={}".format(os.path.join(package_path, tag, "lib")),
            "--prefix={}".format(os.path.join(package_path, tag))] + configure_flags, check=True)
        subprocess.run(["make", "clean"], check=True)
        subprocess.run(["make"] + make_options, check=True)
        subprocess.run(["make", "install"], check=True)
    elif build_system == "cmake":
        build_dir = os.path.join(constituent_path, "package_build")
        configure_flags = [
                "-DCMAKE_INSTALL_LIBDIR=lib",
                "-DCMAKE_INSTALL_PREFIX=" + os.path.join(package_path, tag),
                "-DCMAKE_MAKE_PROGRAM=" + ninja_path,
                ] + configure_flags

        if rpath:
            configure_flags = ["-DCMAKE_INSTALL_RPATH=" + rpath] + configure_flags
        if cmake_prefix_path:
            configure_flags = ["-DCMAKE_PREFIX_PATH=" + ";".join(cmake_prefix_path)] + configure_flags

        try:
            shutil.rmtree(build_dir)
        except FileNotFoundError:
            pass

        print("Using '{}' as configure flags".format(" ".join(configure_flags)))

        os.mkdir(build_dir)
        os.chdir(build_dir)
        subprocess.run([cmake_path, "-G", "Ninja"] + configure_flags + ["../"], check=True)
        subprocess.run(["ninja"] + make_options, check=True)
        subprocess.run(["ninja", "install"], check=True)
    elif build_system == "meson":
        build_dir = os.path.join(constituent_path, "package_build")
        configure_flags = [
                "-Dlibdir=lib",
                "--prefix=" + os.path.join(package_path, tag)
                ] + configure_flags

        if cmake_prefix_path:
            configure_flags = ["--cmake-prefix-path=[" + ",".join("'{0}'".format(w) for w in cmake_prefix_path) + "]"] + \
                    configure_flags

        try:
            shutil.rmtree(build_dir)
        except FileNotFoundError:
            pass

        print("Using '{}' as configure flags".format(" ".join(configure_flags)))

        subprocess.run(["meson"] + configure_flags + [".", build_dir], check=True)
        os.chdir(build_dir)
        subprocess.run(["ninja"] + make_options, check=True)
        subprocess.run(["ninja", "install"], check=True)
        mesonrpath.set_rpath (rpath)
    elif build_system == "scons":
        if configure_flags:
            print("Using '{}' as configure flags".format(" ".join(configure_flags)))
        subprocess.run(["scons",
            "LIBDIR=" + os.path.join(package_path, tag, "lib"),
            "PREFIX=" + os.path.join(package_path, tag)] + configure_flags + make_options, check=True)
        subprocess.run(["scons", "-c"], check=True)
        subprocess.run(["scons", "install"], check=True)

def main():
    package_path = sys.argv[1]
    tag = sys.argv[2]
    constituent = sys.argv[3]
    package_build_path = sys.argv[4]
    pkg_config_path = sys.argv[5]
    if len(sys.argv[6]) > 0:
        rpath = sys.argv[6]
    else:
        rpath = None
    if len(sys.argv[7]) > 0:
        cmake_prefix_path = sys.argv[7].split(':')
    else:
        cmake_prefix_path = []
    if len(sys.argv[8]) > 0:
        make_options = shlex.split(sys.argv[8])
    else:
        make_options = []

    build_path = os.path.join(package_path, tag, "build")
    constituent_path = os.path.join(build_path, constituent)
    sources_path = os.path.join(package_path, "sources")

    build_system = None

    ignore_packages = os.environ.get("PB_IGNORE_PACKAGES")
    if ignore_packages:
        if constituent in ignore_packages.split(":"):
            print("---")
            print("Ignoring installation of '" + constituent + "' according to PB_IGNORE_PACKAGES environment variable")
            print("The system version will be used")
            print("---")
            sys.exit(0)

    try:
        os.mkdir (build_path)
    except FileExistsError:
        pass

    os.environ["PB_PACKAGE_PATH"] = package_path
    os.environ["PB_PACKAGE_INSTALL_PREFIX"] = os.path.join(package_path, tag)
    os.environ["PKG_CONFIG_PATH"] = pkg_config_path
    os.environ["LD_LIBRARY_PATH"] = os.path.join(package_path, tag, "lib")
    os.environ["PATH"] = os.path.join(package_path, tag, "bin") + ":" + os.environ["PATH"]

    if rpath:
        os.environ["LDFLAGS"] = "-Wl,-rpath," + rpath

    # Source extraction

    if not os.path.isfile(os.path.join(build_path, "{}_extract_done".format(constituent))):
        try:
            shutil.rmtree(constituent_path)
            os.remove(os.path.join(build_path, "{}_install_done".format(constituent)))
        except FileNotFoundError:
            pass
        except Exception as error:
            print(error)
            sys.exit(1)

        if os.path.isfile(os.path.join(sources_path, "{}.tar.gz".format(constituent))):
            print("Untar source files using '{}.tar.gz'".format(constituent))
            print("---")
            try:
                os.mkdir(os.path.join(build_path, constituent))
            except FileExistsError:
                pass
            subprocess.run(["tar", "-xvzf", os.path.join(sources_path, "{}.tar.gz".format(constituent)),
                "-C", os.path.join(build_path, constituent), "--strip=1"], check=True)
        elif os.path.isfile(os.path.join(sources_path, "{}.tar.bz2".format(constituent))):
            print("Untar source files using '{}.tar.bz2'".format(constituent))
            print("---")
            try:
                os.mkdir(os.path.join(build_path, constituent))
            except FileExistsError:
                pass
            subprocess.run(["tar", "-xvjf", os.path.join(sources_path, "{}.tar.bz2".format(constituent)),
                "-C", os.path.join(build_path, constituent), "--strip=1"], check=True)
        elif os.path.isfile(os.path.join(sources_path, "{}.tar.xz".format(constituent))):
            print("Untar source files using '{}.tar.xz'".format(constituent))
            print("---")
            try:
                os.mkdir(os.path.join(build_path, constituent))
            except FileExistsError:
                pass
            subprocess.run(["tar", "-xvJf", os.path.join(sources_path, "{}.tar.xz".format(constituent)),
                "-C", os.path.join(build_path, constituent), "--strip=1"], check=True)
        else:
            print("Can't find source file for '{}' in a supported format (gz, bz2 or xz)".format(constituent))
            sys.exit(1)

        patch_files = glob.glob(os.path.join(sources_path, "{}*.patch".format(constituent)))
        for filename in patch_files:
            print("---")
            print("Apply patch file '{}'".format(filename))
            print (" ".join(["patch", "-p1", "-d", constituent_path, "-i", filename]))
            subprocess.run(["patch", "-p1", "-d", constituent_path, "-i", filename], check=True)

        for root, dirs, files in os.walk(constituent_path):
            for d in dirs:
                os.chmod(os.path.join(root, d),
                        os.stat(os.path.join(root,d)).st_mode | stat.S_IWGRP | stat.S_IRGRP)
            for f in files:
                os.chmod(os.path.join(root, f),
                        os.stat(os.path.join(root,f)).st_mode | stat.S_IWGRP | stat.S_IRGRP)

        pathlib.Path(os.path.join(build_path, "{}_extract_done".format(constituent))).touch()

    # Configuration and compilation

    if not os.path.isfile(os.path.join(build_path, "{}_install_done".format(constituent))):
        build_system_file = os.path.join(sources_path, "{}.build.system".format(constituent)) 

        os.chdir(constituent_path)

        pre_configure_script = os.path.join(sources_path, "{}.pre-configure.sh".format(constituent))
        if os.path.isfile(pre_configure_script):
            print("---")
            print("Running '{}'".format(pre_configure_script))
            subprocess.run (["sh", pre_configure_script], check=True)

        if os.path.isfile(build_system_file):
            with open(build_system_file) as f:
                build_system = f.readline().strip()
        elif os.path.isfile(os.path.join(constituent_path, "meson.build")):
                build_system = 'meson'
        elif os.path.isfile(os.path.join(constituent_path, "CMakeLists.txt")):
                build_system = 'cmake'
        elif os.path.isfile(os.path.join(constituent_path, "configure")):
                build_system = 'autotools'
        elif os.path.isfile(os.path.join(constituent_path, "SConstruct")):
                build_system = 'scons'

        if not build_system in BUILD_SYSTEMS:
            print("'{0}' is not in the supported build system list [{1}]".format(build_system, ",".join(BUILD_SYSTEMS)))
            sys.exit(1)

        make_options_file = os.path.join(sources_path, "{}.make.options".format(constituent))
        if os.path.isfile(make_options_file):
            with open(make_options_file) as f:
                make_options = shlex.split(os.path.expandvars(f.readline().strip()))

        print("---")
        print("Using '{}' as make options".format(" ".join(make_options)))
        print("Using '{}' build system".format(build_system))

        options_file = os.path.join(sources_path, "{}.options".format(constituent))
        if os.path.isfile(options_file):
            with open(options_file) as f:
                for line in f.readlines():
                    configure_flags = shlex.split(os.path.expandvars(line.strip()))

                    os.chdir(constituent_path)
                    print("---")
                    build(build_system, constituent_path, package_path, tag, configure_flags, make_options, cmake_prefix_path, rpath)
        else:
            os.chdir(constituent_path)
            print("---")
            build(build_system, constituent_path, package_path, tag, [], make_options, cmake_prefix_path, rpath)


        os.chdir(constituent_path)

        pos_make_script = os.path.join(sources_path, "{}.post-make.sh".format(constituent))
        if os.path.isfile(pos_make_script):
            print("---")
            print("Running '{}'".format(pos_make_script))
            subprocess.run (["sh", pos_make_script, package_path, tag, constituent, pkg_config_path], check=True)

        if build_system == "autotools":
            log_file=os.path.join(constituent_path, "config.log")
        elif build_system == "cmake":
            log_file=os.path.join(constituent_path, "package_build", "CMakeFiles", "CMakeOutput.log")
        elif build_system == "meson":
            log_file=os.path.join(constituent_path, "package_build", "meson-logs", "meson-log.txt")
        else:
            log_file = None

        if log_file:
            try:
                shutil.copyfile(log_file, os.path.join(build_path, "{}.build.log".format(constituent)))
            except FileNotFoundError:
                pass

        pathlib.Path(os.path.join(build_path, "{}_install_done".format(constituent))).touch()

        print ('---')

        if not os.environ.get("PB_NO_BUILD_CLEANUP"):
            shutil.rmtree(constituent_path)

main()
