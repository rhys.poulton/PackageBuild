#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-2.0-or-later

import sys
import os
import optparse
import shutil
import subprocess
import json
import shlex
import mesonrpath

def main():

    parser = optparse.OptionParser(add_help_option=False)
    parser.add_option ('--rpath', action='store', type='string', default=None)
    parser.add_option ('--cmake-prefix-path', action='store', type='string', default=None)
    parser.add_option ('--meson-options', action='store', type='string', default=None)
    parser.add_option ('--cmake-options', action='store', type='string', default=None)

    options, arguments = parser.parse_args ()

    if (len(arguments) < 5):
        print("Missing parameters")
        sys.exit(1)

    package_path = arguments[0]
    tag = arguments[1]
    build_system = arguments[2]
    package_build_path = arguments[3]
    pkg_config_path = arguments[4]

    if options.cmake_prefix_path and len(options.cmake_prefix_path) > 0:
        meson_cmake_prefix_path = ",".join("'{0}'".format(w) for w in options.cmake_prefix_path.split(":"))
        cmake_prefix_path = ";".join(options.cmake_prefix_path.split(":"))
    else:
        meson_cmake_prefix_path = None
        cmake_prefix_path = None

    if options.meson_options and len(options.meson_options) > 0:
        meson_options = options.meson_options
    else:
        meson_options = None

    if options.cmake_options and len(options.cmake_options) > 0:
        cmake_options = options.cmake_options
    else:
        cmake_options = None

    if build_system == 'meson':
        configure_options = meson_options
        if not os.path.isfile (os.path.join (package_path, "meson.build")):
            print ("meson.build file not found")
            sys.exit (1)
    elif build_system == 'cmake':
        configure_options = cmake_options
        if not os.path.isfile (os.path.join (package_path, "CMakeLists.txt")):
            print ("CMakeLists.txt file not found")
            sys.exit (1)
    else:
        print ("Build system not supported")
        sys.exit (1)

    verbose = os.environ.get("VERBOSE")
    build_type = os.environ.get("PB_BUILD_TYPE")

    build_path = os.path.join (package_path, tag + "-" + build_system)

    os.chdir (package_path)

    os.environ["PB_PACKAGE_PATH"] = package_path
    os.environ["PB_PACKAGE_INSTALL_PREFIX"] = os.path.join(package_path, tag)
    os.environ["PKG_CONFIG_PATH"] = pkg_config_path
    os.environ["PATH"] = os.path.join(package_path, tag, "bin") + ":" + os.environ["PATH"]

    if options.rpath:
        os.environ["LDFLAGS"] = "-Wl,-rpath," + options.rpath

    cmake_path = shutil.which("cmake3")
    if not cmake_path:
        cmake_path = shutil.which("cmake")
    ninja_path = shutil.which("ninja")
    meson_path = shutil.which("meson")

    if not ninja_path:
        print("ninja not found in {}".format(os.environ["PATH"]))
        sys.exit(1)

    print ("Using " + build_system + " build system")

    if options.rpath:
        print ("Using '" + options.rpath + "' as rpath")

    if verbose:
        if build_system == "cmake":
            print ("With cmake from '{}'".format(cmake_path))
        elif build_system == "meson":
            print ("With meson from '{}'".format(meson_path))

        print ("With ninja from '{}'".format(ninja_path))

    configure_flags = []
    if configure_options:
        configure_flags += shlex.split(os.path.expandvars(configure_options))

    if build_system == "meson":
        if meson_cmake_prefix_path:
            configure_flags = ["--cmake-prefix-path=[" + meson_cmake_prefix_path + "]"] +  configure_flags;

        configure_flags = [
                "--prefix=" + os.path.join (package_path, tag),
                "-Dlibdir=lib",
                "-Dbuildtype=debugoptimized"] + configure_flags

        if build_type:
            configure_flags += ["-Dbuildtype=" + build_type]

        if not meson_path:
            print("meson not found in {}".format(os.environ["PATH"]))
            sys.exit(1)

    elif build_system == "cmake":
        if options.rpath:
            configure_flags = ["-DCMAKE_INSTALL_RPATH=" + options.rpath] + configure_flags
        if cmake_prefix_path:
            configure_flags = ["-DCMAKE_PREFIX_PATH=" + cmake_prefix_path] + configure_flags

        configure_flags = [
            "-DCMAKE_INSTALL_PREFIX=" + os.path.join(package_path, tag),
            "-DCMAKE_INSTALL_LIBDIR=lib",
            "-DCMAKE_BUILD_TYPE=RelWithDebInfo",
            "-DCMAKE_MAKE_PROGRAM=" + ninja_path] + configure_flags

        if build_type:
            configure_flags += ["-DCMAKE_BUILD_TYPE=" + build_type]

        if not cmake_path:
            print("cmake not found in {}".format(os.environ["PATH"]))
            sys.exit(1)

    else:
        print ("Unsupported build system")
        sys.exit(1)

    configure_flag_string = " ".join(configure_flags)
    print ("Using '{}' as configure flags".format(configure_flag_string))

    try:
        with open(os.path.join(build_path, "pb_configure_flags")) as f:
            line = f.readline().strip()
            configure_flag_changed = line != configure_flag_string
    except:
        configure_flag_changed = True
        pass

    try:
        if configure_flag_changed or not os.path.isdir(build_path):

            if configure_flag_changed:
                print ("Configuration flags have changed")
                try:
                    shutil.rmtree (os.path.join (build_path))
                except FileNotFoundError:
                    pass

            if build_system == "meson":
                subprocess.run([meson_path] + configure_flags + [package_path, build_path], check=True)

            elif build_system == "cmake":
                os.mkdir(build_path)
                os.chdir(build_path)
                subprocess.run ([cmake_path, "-G", "Ninja"] + configure_flags + [package_path], check=True)

            else:
                print ("Unsupported build system")
                sys.exit(1)

        os.chdir(build_path)

        with open(os.path.join(build_path, "pb_configure_flags"), "w") as f:
            f.write(configure_flag_string)

    except Exception as error:
        print(error)
        try:
            shutil.rmtree (os.path.join (build_path))
        except FileNotFoundError:
            pass
        sys.exit(1)

    try:
        if verbose:
            subprocess.run ([ninja_path, "-v", "install"], check=True)
        else:
            subprocess.run ([ninja_path, "install"], check=True)

        if build_system == "meson" and options.rpath:
            mesonrpath.set_rpath(options.rpath)

    except Exception as error:
        print(error)
        sys.exit(1)

if __name__ == '__main__':
    main()
