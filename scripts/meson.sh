#!/bin/sh

# SPDX-License-Identifier: GPL-2.0-or-later

root=$1
tag=$2
meson_version=$3

set -e

if [ -e "${root}/${tag}/bin/meson" ]
then
	exit 0
fi

if [ ! -f "${root}/sources/${meson_version}.tar.gz" ]
then
	echo "${meson_version}.tar.gz not found !"
	exit 1
fi

rm -rf "${root}/${tag}/${meson_version}"
tar -xvzf "${root}/sources/${meson_version}.tar.gz" -C "${root}/${tag}/"

if [ -f "${root}/sources/meson.patch" ]
then
	patch -p1 -d "${root}/${tag}/${meson_version}" < "${root}/sources/meson.patch"
fi

rm -f "${root}/${tag}/bin/meson"
ln -s "${root}/${tag}/${meson_version}/meson.py" "${root}/${tag}/bin/meson"
