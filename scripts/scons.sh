#!/bin/sh

# SPDX-License-Identifier: GPL-2.0-or-later

root=$1
tag=$2
scons_version=$3

set -e

scons_tarball="${scons_version}.tar.gz"

if [ -e ${root}/${tag}/bin/scons ]
then
	exit 0
fi

if [ ! -f "${root}/sources/${scons_tarball}" ]
then
	echo "${scons_tarball} not found !"
	exit 1
fi

rm -rf "${root}/${tag}/scons"
mkdir "${root}/${tag}/scons"
tar -xvzf "${root}/sources/${scons_tarball}" -C "${root}/${tag}/scons"

rm -f ${root}/${tag}/bin/scons
ln -s ${root}/${tag}/scons/scons.py ${root}/${tag}/bin/scons
