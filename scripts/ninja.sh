#!/bin/sh

# SPDX-License-Identifier: GPL-2.0-or-later

root=$1
tag=$2
ninja_version=$3

if [ "$#" -eq 4 ]
then
	rpath=$4
fi

set -e

ninja_tarball="${ninja_version}.tar.gz"

if [ -e ${root}/${tag}/bin/ninja ]
then
	exit 0
fi

if [ ! -f "${root}/sources/${ninja_tarball}" ]
then
	echo "${ninja_tarball} not found !"
	exit 1
fi

rm -rf "${root}/${tag}/${ninja_version}"
tar -xvzf "${root}/sources/${ninja_tarball}" -C "${root}/${tag}"

cd ${root}/${tag}

mkdir -p build/ninja
mkdir -p bin

cd build/ninja

if [ ! -z ${rpath+x} ]
then
	export LDFLAGS="-Wl,-rpath,${rpath}"
fi

export CFLAGS=-Wno-implicit-fallthrough
${root}/${tag}/${ninja_version}/configure.py --bootstrap
cp ninja ${root}/${tag}/bin/ninja
rm -rf ${root}/${tag}/build/ninja
rm -rf ${root}/${tag}/${ninja_version}
